# NAME

passgen - Password Generator

# SYNOPSIS

**passgen** \[**LENGTH**\] \[**CHARSET-OPTIONS**\]

# DESCRIPTION

**passgen** is a small utility that generates a \'random\' password from
a set of charsets (see NOTES). These can be toggled with the appropriate
\--enable arguments, which collectively are called CHARSET-OPTIONS
above. Per default only ASCII is enabled.

# OPTIONS

**LENGTH**

:   Generate a password of length LENGTH (Default: 26)

**\--enable-latin1**

:   Enable the Latin1 charset

**\--enable-latin1-{symbols, upper, lower}**

:   Enable the Latin1 {Symbols, Upper, Lower} charset

**\--enable-latin-ext-a**

:   Enable the Latin Extended A charset

**\--enable-latin-ext-a-{upper, lower}**

:   Enable the Latin Extened A {Upper, Lower} charset

**\--enable-latin-ext-b**

:   Enable the Latin Extended B charset

**\--enable-latin-ext-b-{upper, lower}**

:   Enable the Latin Extened B {Upper, Lower} charset

**\--enable-all**

:   Enable all charsets **\--disable-ascii** Disable the ASCII charset

**\--disable-ascii-{upper, lower, digits, symbols}**

:   Disable the ASCII {Upper, Lower, Digits, Symbols} Charset

# NOTES

Possible characters inside the password currently are ASCII, Latin1,
Latin Extended A and Latin Extended B (all code points, other than the
control characters).

# AUTHOR

zocker
