#include <stdio.h>
#include <stdlib.h>

static char *s = "A";
static int stop = 0;

void next(void);
void next(void)
{
	char c = s[1];
	if (c == (char)0xFF) {
		stop = 1;
	} else {
		s[1] = ++c;
	}
}

int main(int argc, char **argv)
{
	if (argc < 3)
		return 1;

	s = argv[1];
	unsigned long long len = atoll(argv[2]);

	if (!len) {
		return 1;
	}

	char cnt = (char)0;
	for (unsigned long long i = 0; i < len; ++i) {
		printf("\"%s\", ", s);
		next();
		++cnt;
		if (stop)
			break;

		if (cnt == (char)7) {
			cnt = (char)0;
			printf("\t\\\n");
		}
	}

	printf("NULL");
	printf("\n");

	return 0;
}
