CC ?= gcc

CFLAGS += -march=native -O2 -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=2 \
	  -fstack-protector-strong -fcf-protection -fpie \
	  -fPIC -pedantic -pedantic-errors \
	  -fno-delete-null-pointer-checks -Wall -Wextra \
	  -std=c99 -D_XOPEN_SOURCE=600

LDFLAGS += -Wl,-z,defs -Wl,-z,now -Wl,-z,relro -Wl,-z,nodlopen -Wl,-z,noexecstack

DESTDIR ?= /usr/local
MANPREFIX ?= /share/man

BIN_DIR ?= $(DESTDIR)$(PREFIX)/bin
ZSH_SITE_FUNCTIONS ?= $(DESTDIR)/share/zsh/site-functions
MAN1_DIR ?= $(DESTDIR)$(MANPREFIX)/man1

passgen: passgen.c charsets.h
	@echo CC $<
	@$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $<

passgen-genmap: genmap.c
	@echo CC $<
	@$(CC) $(CFLAGS) -o $@ $<

.PHONY: install
install: passgen
	mkdir -p $(BIN_DIR)
	mkdir -p $(MAN1_DIR)
	install -m755 passgen  $(BIN_DIR)
	install -m644 passgen.1 $(MAN1_DIR)
	install -m644 passgen.zsh-completions $(ZSH_SITE_FUNCTIONS)/_passgen

.PHONY: all
all: passgen passgen-genmap

.PHONY: clean
clean:
	rm -f passgen passgen.o

.PHONY: help
help:
	@echo "passgen: build passgen"
	@echo "passgen-genmap: build the genmap util"
	@echo "all: build passgen and util"
	@echo "install: install binaries to $(BIN_DIR), man pages to $(MAN1_DIR) and zsh completions to $(ZSH_SITE_FUNCTIONS)"
	@echo "clean: remove generated files"
	@echo "CC: $(CC)"
	@echo "CFLAGS: $(CFLAGS)"
	@echo "LDFLAGS: $(LDFLAGS)"
