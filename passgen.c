#include <stdio.h>
#include <sys/random.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>
#include "charsets.h"

// Prog defines
#define PROG_NAME "passgen"
#define PROG_VERSION "0.2.4"
#define PROG_AUTHOR "zocker"
#define PROG_MAIL "zocker@10zen.eu"

// Function defines
#define PRINT(x, y)                            \
	if (x[0])                              \
		printf("%s", random_entry(x)); \
	else                                   \
		++y

#define MAP_NAME(x, y) x##_##y##_MAP
#define CASE(x, y, z)                     \
	case x##_##y:                     \
		PRINT(MAP_NAME(x, y), z); \
		break
#define SET_BIT(x, y) x = x | y
#define CHECK_BIT(x, y) ((x & y) == y)
#define CHECK_MAP(a, b, c, d)      \
	if (CHECK_BIT(a, b)) {     \
		USE_CHARSET(c, d); \
	}

// Bit Defines
#define BIT_ASCII_UPPER 0x1
#define BIT_ASCII_LOWER 0x2
#define BIT_ASCII_DIGITS 0x4
#define BIT_ASCII_SYMBOLS 0x8
#define BIT_LATIN1_SYMBOLS 0x10
#define BIT_LATIN1_UPPER 0x20
#define BIT_LATIN1_LOWER 0x40
#define BIT_LATIN_EXT_A_UPPER 0x80
#define BIT_LATIN_EXT_A_LOWER 0x100
#define BIT_LATIN_EXT_B_UPPER 0x200
#define BIT_LATIN_EXT_B_LOWER 0x400
#define BIT_ASCII                                               \
	(BIT_ASCII_UPPER | BIT_ASCII_LOWER | BIT_ASCII_DIGITS | \
	 BIT_ASCII_SYMBOLS)
#define BIT_LATIN1 (BIT_LATIN1_SYMBOLS | BIT_LATIN1_UPPER | BIT_LATIN1_LOWER)
#define BIT_LATIN_EXT_A (BIT_LATIN_EXT_A_UPPER | BIT_LATIN_EXT_A_LOWER)
#define BIT_LATIN_EXT_B (BIT_LATIN_EXT_B_UPPER | BIT_LATIN_EXT_B_LOWER)
#define BIT_LATIN_EXT (BIT_LATIN_EXT_A | BIT_LATIN_EXT_A)
#define BIT_ALL (BIT_LATIN1 | BIT_LATIN_EXT)

// Default defines and typedefs
#define SEED_SIZE 512
#define DEFAULT_LENGTH 26
typedef uint16_t bitmap;

// Functions and stucts/enums
char *random_entry(char **);
struct options *parse(int, char **);
int cmpstr(char *, char *);
void usage(void);

struct options {
	bitmap maps;
	uint64_t len;
	char help_flag;
};

enum {
	ASCII_UPPER,
	ASCII_LOWER,
	ASCII_DIGITS,
	ASCII_SYMBOLS,
	LATIN1_SYMBOLS,
	LATIN1_UPPER,
	LATIN1_LOWER,
	LATIN_EXT_A_UPPER,
	LATIN_EXT_A_LOWER,
	LATIN_EXT_B_UPPER,
	LATIN_EXT_B_LOWER,
	NUMBER_OF_CHARSETS
};

/**
 * random_entry() - Return a random entry of an array
 *
 * @map: An array of char pointers
 *
 * Using the fact that we terminate all arrays with an explicit NULL, we
 * determine the size of @map by iterating until we hit that NULL and use that
 * size to get a random entry from @map.
 *
 * Returns:
 * * A random entry from @map
 * * NULL if the size of @map is 0
 */
char *random_entry(char **map)
{
	uint64_t size = 0;

	for (; map[size] != NULL; ++size)
		;

	if (!size)
		return NULL;

	return map[random() % size];
}

/**
 * parse() - Parse argv for any options
 *
 * @argc: Number of arguments
 * @argv: Arguments
 *
 * We iterate over all arguments and determine from them which charsets should
 * be used and how long the password that we generate should be. If there is
 * only one argument (that we assume to be the name of the program), then we
 * skip parsing and jump to the end of the function.
 *
 * Returns:
 * * A struct options with the parsed values
 * * NULL on any error
 */
struct options *parse(int argc, char **argv)
{
	struct options *o = calloc(1, sizeof(struct options));

	if (!o) {
		perror("passgen");
		return NULL;
	}

	o->maps = 0x0;
	o->len = 0;
	char *len = NULL;

	if (argc < 2)
		goto skip_parsing;

	for (int i = 1; i < argc; ++i) {
		if (cmpstr(argv[i], "--disable-ascii")) {
			SET_BIT(o->maps, BIT_ASCII);
		} else if (cmpstr(argv[i], "--disable-ascii-upper")) {
			SET_BIT(o->maps, BIT_ASCII_UPPER);
		} else if (cmpstr(argv[i], "--disable-ascii-lower")) {
			SET_BIT(o->maps, BIT_ASCII_LOWER);
		} else if (cmpstr(argv[i], "--disable-ascii-digits")) {
			SET_BIT(o->maps, BIT_ASCII_DIGITS);
		} else if (cmpstr(argv[i], "--disable-ascii-symbols")) {
			SET_BIT(o->maps, BIT_ASCII_SYMBOLS);
		} else if (cmpstr(argv[i], "--enable-latin1")) {
			SET_BIT(o->maps, BIT_LATIN1);
		} else if (cmpstr(argv[i], "--enable-latin1-symbols")) {
			SET_BIT(o->maps, BIT_LATIN1_SYMBOLS);
		} else if (cmpstr(argv[i], "--enable-latin1-upper")) {
			SET_BIT(o->maps, BIT_LATIN1_UPPER);
		} else if (cmpstr(argv[i], "--enable-latin1-lower")) {
			SET_BIT(o->maps, BIT_LATIN1_LOWER);
		} else if (cmpstr(argv[i], "--enable-latin-ext")) {
			SET_BIT(o->maps, BIT_LATIN_EXT);
		} else if (cmpstr(argv[i], "--enable-latin-ext-a")) {
			SET_BIT(o->maps, BIT_LATIN_EXT_A);
		} else if (cmpstr(argv[i], "--enable-latin-ext-a-upper")) {
			SET_BIT(o->maps, BIT_LATIN_EXT_A_UPPER);
		} else if (cmpstr(argv[i], "--enable-latin-ext-a-lower")) {
			SET_BIT(o->maps, BIT_LATIN_EXT_A_LOWER);
		} else if (cmpstr(argv[i], "--enable-latin-ext-b")) {
			SET_BIT(o->maps, BIT_LATIN_EXT_B);
		} else if (cmpstr(argv[i], "--enable-latin-ext-b-upper")) {
			SET_BIT(o->maps, BIT_LATIN_EXT_B_UPPER);
		} else if (cmpstr(argv[i], "--enable-latin-ext-b-lower")) {
			SET_BIT(o->maps, BIT_LATIN_EXT_B_LOWER);
		} else if (cmpstr(argv[i], "--enable-all")) {
			SET_BIT(o->maps, BIT_ALL);
		} else if (cmpstr(argv[i], "-h") || cmpstr(argv[i], "--help")) {
			o->help_flag = 1;
			goto skip_parsing;
		} else {
			len = argv[i];
		}
	}

	char *tmp;
	if (len)
		o->len = strtoull(len, &tmp, 0);

skip_parsing:

	if (!o->len || tmp == len || errno != 0)
		o->len = DEFAULT_LENGTH;

	return o;
}

/**
 * cmpstr() - Compare two strings
 *
 * @a: The first string
 * @b: The second string
 *
 * We compare eachy byte of @a and @b to determine if they are equal, however
 * unlike the standard strcmp() function we don't compare if @one is less or
 * greater than @two.
 *
 * We also check if the first char is '-', because if it is not, then it can't
 * be an argument.
 *
 * Returns:
 * * 1 if the strings are equal
 * * 0 if they are not
 */
int cmpstr(char *a, char *b)
{
	if (a[0] != '-')
		return 0;

	while (*a != '\0') {
		if (*b == '\0')
			return 0;

		if (*a != *b)
			return 0;

		++a;
		++b;
	}

	if (*b != '\0')
		return 0;

	return 1;
}

/**
 * usage() - Print usage information
 *
 * Print Usage information and charset options, as well as program name, version,
 * author, mail-address and license.
 */
void usage(void)
{
	printf("%s-%s\n", PROG_NAME, PROG_VERSION);
	printf("Copyright (C) 2024 %s (%s)\n", PROG_AUTHOR, PROG_MAIL);
	printf("License: GPL-3.0-or-later\n");
	printf("Usage: %s [LENGTH] [CHARSET-OPTIONS]\n", PROG_NAME);
	printf("Available charset options:\n");
	printf("--disable-ascii *\n");
	printf("--disable-ascii-upper\n");
	printf("--disable-ascii-lower\n");
	printf("--disable-ascii-digits\n");
	printf("--disable-ascii-symbols\n");
	printf("--enable-latin1 *\n");
	printf("--enable-latin1-upper\n");
	printf("--enable-latin1-lower\n");
	printf("--enable-latin1-symbols\n");
	printf("--enable-latin-ext *\n");
	printf("--enable-latin-ext-a *\n");
	printf("--enable-latin-ext-a-upper\n");
	printf("--enable-latin-ext-a-lower\n");
	printf("--enable-latin-ext-b *\n");
	printf("--enable-latin-ext-b-upper\n");
	printf("--enable-latin-ext-b-lower\n");
	printf("\nOptions marked with an asterisk are composite. They enable/disable all options that use it as a base.\n");
	printf("That is '--enable-latin1' is equivalent to '--enable-latin1-upper --enable-latin1-lower --enable-latin1-symbols'\n");
}

/**
 * main() - The main function
 *
 * @argc: Number of arguments
 * @argv: Arguments
 *
 * We first parse the options, then we seed srandom, then we apply the charset
 * settings that were chosen and then generate the password that is, at the end,
 * printed to stdout.
 *
 * Returns:
 * * 0, if exited successfuint64_ty
 * * 1, in all other cases
 */
int main(int argc, char **argv)
{
	struct options *o = parse(argc, argv);

	if (!o)
		return 1;

	if (o->help_flag) {
		usage();
		free(o);
		return 0;
	}

	char *s = calloc(SEED_SIZE + 1, sizeof(char));

	if (!s) {
		perror("passgen");

		goto error;
	}

	if ((getrandom(s, SEED_SIZE, GRND_NONBLOCK)) == -1) {
		perror("passgen");

		goto error;
	}

	srandom(*(unsigned int *)s);
	free(s);

	char *ASCII_UPPER_MAP[] = ASCII_UPPER_CHARSET;
	char *ASCII_LOWER_MAP[] = ASCII_LOWER_CHARSET;
	char *ASCII_DIGITS_MAP[] = ASCII_DIGITS_CHARSET;
	char *ASCII_SYMBOLS_MAP[] = ASCII_SYMBOLS_CHARSET;
	char *LATIN1_SYMBOLS_MAP[] = LATIN1_SYMBOLS_CHARSET;
	char *LATIN1_UPPER_MAP[] = LATIN1_UPPER_CHARSET;
	char *LATIN1_LOWER_MAP[] = LATIN1_LOWER_CHARSET;
	char *LATIN_EXT_A_UPPER_MAP[] = LATIN_EXT_A_UPPER_CHARSET;
	char *LATIN_EXT_A_LOWER_MAP[] = LATIN_EXT_A_LOWER_CHARSET;
	char *LATIN_EXT_B_UPPER_MAP[] = LATIN_EXT_B_UPPER_CHARSET;
	char *LATIN_EXT_B_LOWER_MAP[] = LATIN_EXT_B_LOWER_CHARSET;

	if (o->maps == BIT_ASCII)
		o->maps = 0;

	CHECK_MAP(o->maps, BIT_ASCII_UPPER, ASCII_UPPER_MAP, ASCII_UPPER);
	CHECK_MAP(o->maps, BIT_ASCII_LOWER, ASCII_LOWER_MAP, ASCII_LOWER);
	CHECK_MAP(o->maps, BIT_ASCII_DIGITS, ASCII_DIGITS_MAP, ASCII_DIGITS);
	CHECK_MAP(o->maps, BIT_ASCII_SYMBOLS, ASCII_SYMBOLS_MAP, ASCII_SYMBOLS);
	CHECK_MAP(o->maps, BIT_LATIN1_SYMBOLS, LATIN1_SYMBOLS_MAP,
		  LATIN1_SYMBOLS);
	CHECK_MAP(o->maps, BIT_LATIN1_UPPER, LATIN1_UPPER_MAP, LATIN1_UPPER);
	CHECK_MAP(o->maps, BIT_LATIN1_LOWER, LATIN1_LOWER_MAP, LATIN1_LOWER);
	CHECK_MAP(o->maps, BIT_LATIN_EXT_A_UPPER, LATIN_EXT_A_UPPER_MAP,
		  LATIN_EXT_A_UPPER);
	CHECK_MAP(o->maps, BIT_LATIN_EXT_A_LOWER, LATIN_EXT_A_LOWER_MAP,
		  LATIN_EXT_A_LOWER);
	CHECK_MAP(o->maps, BIT_LATIN_EXT_B_UPPER, LATIN_EXT_B_LOWER_MAP,
		  LATIN_EXT_B_UPPER);
	CHECK_MAP(o->maps, BIT_LATIN_EXT_B_LOWER, LATIN_EXT_B_UPPER_MAP,
		  LATIN_EXT_B_LOWER);

	uint64_t i = o->len;
	free(o);

	for (; i > 0; --i) {
		switch (random() % NUMBER_OF_CHARSETS) {
		CASE(ASCII, UPPER, i);
		CASE(ASCII, LOWER, i);
		CASE(ASCII, DIGITS, i);
		CASE(ASCII, SYMBOLS, i);
		CASE(LATIN1, SYMBOLS, i);
		CASE(LATIN1, UPPER, i);
		CASE(LATIN1, LOWER, i);
		CASE(LATIN_EXT_A, LOWER, i);
		CASE(LATIN_EXT_A, UPPER, i);
		CASE(LATIN_EXT_B, LOWER, i);
		CASE(LATIN_EXT_B, UPPER, i);
		default:
			break;
		}
	}

	printf("\n");

	return EXIT_SUCCESS;

error:
	fprintf(stderr, "%s [LENGTH]\n", argv[0]);
	free(o);
	free(s);
	return EXIT_FAILURE;
}
